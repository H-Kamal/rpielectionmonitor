# Parses Wikipedia page to see the latest polling.
# https://en.wikipedia.org/wiki/Opinion_polling_for_the_2019_Canadian_federal_election

import requests
import csv

S = requests.Session()

# The API endpoint to the English Wikipedia API
URL = "https://en.wikipedia.org/w/api.php"

# What's the title? I think it might be the title of the wikipedia page
TITLE = "Opinion polling for the 2019 Canadian federal election"

# NOTE: Dont have the section : 5
PARAMS = {
	"action" : "parse",
	"page" : TITLE,
	"prop" : "wikitext",
	"format" : "json"

}

def get_table():
	# Parse the page, get its table data and save it into a csv
	res = S.get(url = URL, params = PARAMS)
	data = res.json()
	wikitext = data['parse']['wikitext']['*']

	# Each row in the table is split up by |-
	# You can see this by looking at the edit tab of each Wiki page
	lines = wikitext.split("|-")

	entries = []
	for line in lines:
		line  = line.strip()
		if line.startswith("|"):
			lineContents = line.split("||")
			for content in lineContents:
				date = content.find("dts|")
				if date is not -1:
					print(content[date+4:-3])
def main():
	get_table()

if (__name__ == '__main__'):
	main()